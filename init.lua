local lgi = require 'lgi'
local Gio = lgi.Gio
local CBuffer = require "tcprompt.cbuffer"

local GREETING = [==[Welcome to awesome!]==]
local PROMPT = "> "
local PROMPT_CONT = ">> "
local PRINT_BUFFER_SIZE = 1000	   -- buffer for print strings
local INPUT_BUFFER_SIZE = 1000	   -- buffer for user input

local lenv = {}
setmetatable(lenv, {__index=_G})


local print_buffer = CBuffer(PRINT_BUFFER_SIZE)

-- overwrite global print function
function lenv.print(...)
   local args = table.pack(...)
   local line = string.format("%s", args[1])
   for i=2,args.n do
      line = string.format("%s\t%s", line, args[i])
   end
   print_buffer:push(line)
end

local function new(args)
   args = args or {}
   local port = args.port or 9999
   local address = args.address or "127.0.0.1"
   local greeting = args.greeting or GREETING
   local prompt = args.prompt or PROMPT
   local prompt_cont = args.prompt_cont or PROMPT_CONT
   local buffer_size = args.buffer_size or INPUT_BUFFER_SIZE

   local service = Gio.SocketService.new()
   service:add_address(Gio.InetSocketAddress.new_from_string(address, port),
		       "STREAM", "TCP")

   local function get_message(connection,   -- TCP connection
			      istream,	    -- Input stream
			      ostream,	    -- Output stream
			      input_buffer, -- User input buffer
			      continue)	    -- Continue multiline input
      -- show prompt symbol
      ostream:async_write(continue and prompt_cont or prompt)
      continue = false -- reset `continue' flag
      local bytes = istream:async_read_bytes(4096)
      if bytes:get_size() > 0 then
	 input_buffer:push(bytes.data)
	 local func, err = load(input_buffer:iterate(),
				nil, "t", lenv) -- load user input
	 if err == nil then
	    -- load successfully
	    -- Clear input buffer, call function and return results
	    input_buffer:clear()
	    local rv = table.pack(pcall(func))
	    -- flush print buffer
	    for l in print_buffer:iterate() do
	       ostream:async_write(string.format("%s\n", l))
	    end
	    print_buffer:clear()
	    if rv[1] ~= true  then
	       -- error during call
	       ostream:async_write(string.format("Runtime error: %s\n", rv[2]))
	    end
	    if rv[2] ~= nil then
	       -- collect all return arguments and print back
	       local msg = rv[2]
	       for i=3,#rv do
		  msg = msg .. "\t" .. rv[i]
	       end
	       ostream:async_write(string.format("%s\n", msg))
	    end
	 elseif err:find("<eof>") ~= nil then
	    -- multiline input continue
	    continue = true
	 else
	    ostream:async_write(string.format("Parse error: %s\n", err))
	    input_buffer:clear()
	 end
	 -- bytes.data
	 Gio.Async.start(get_message)(connection,
				      istream,
				      ostream,
				      input_buffer,
				      continue)
      else
	 connection:close()
      end
   end

   function service:on_incoming(connection)
      local istream = connection:get_input_stream()
      local ostream = connection:get_output_stream()
      local input_buffer = CBuffer(buffer_size)
      -- greet user
      ostream:write(greeting .. "\n")
      Gio.Async.start(get_message)(connection,
				   istream,
				   ostream,
				   input_buffer,
				   false)
      return false
   end
   service:start()
end

return new
