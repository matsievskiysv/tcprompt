%tcprompt

# TCPrompt #

*TCPrompt* stands for *TCP prompt*. This is a simple prompt for [awesomewm](https://awesomewm.org/) session. It allows user to connect to running awesome Lua session and examine (and probably mutate) global variables. It supports multiline input.

## Configuration ##

Configuration of *TCPrompt* is very simple.

Clone repository to you awesome configuration folder
```bash
> git clone https://gitlab.com/matsievskiysv/tcprompt ~/.config/aweseomewm/tcprompt
```
or add git submodule if you use version control for your settings
```bash
> git submodule add https://gitlab.com/matsievskiysv/tcprompt
```

Add one line you `rc.lua` file
```lua
require("tcprompt")()
```

You can change address and port this way:
```lua
local tcprompt = require("tcprompt")
tcprompt{address="0.0.0.0", port=1234}
```

Full list of configuration arguments:

  * `address` -- TCP address. Defaults to `127.0.0.1`
  * `port` -- TCP port. Defaults to `9999`
  * `greeting` -- greeting line. Defaults to `Welcome to awesome!`
  * `prompt` -- prompt string. Defaults to `> `
  * `prompt_cont` -- prompt multiline continuation string. Defaults to `>> `
  * `buffer_size` -- maximum number of input lines. Defaults to 1000


## Running TCPrompt ##

You may connect to *TCPrompt* using [netcat](https://nc110.sourceforge.io/) tool (It may be called `nc`, `ncat` or `netcat` on your system).
The command is
```bash
> ncat 127.0.0.1 9999
```
Now you may type code more or less like you would in Lua REPL. You can define functions
```lua
function test(a, b, c) return c, b, a end
```
you can run them and print output
```lua
print(test(1, 2, 3))
```
You may split your code over multiple lines:
```lua
do
   local R=2.2
   print(math.pi*R^2)
end
```
you can access awesome data structures
```lua
print(awesome.release)
```
you can even spawn programs
```lua
awesome.spawn("xterm")
```
*TCPrompt* operates in global environment, so you only have access to globally defined variables. You can print list of global variables like this
```lua
for i,j in pairs(_G) do print(i, j) end
```

## Limitations ##

- *TCPrompt* has shared buffer for printed messages.
